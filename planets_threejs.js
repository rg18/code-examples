/*przykładowy plik z użyciem biblioteki three.js,
skrypt ten został uzyty na stronie malykopernik.pl,
tworzy planety jako obiekty 3d obracające się w przestrzeni trójwymiarowej
 */
//Renderer Elements
var container = document.createElement('div');
container.setAttribute("id", "canvas");
document.getElementById("slider0").appendChild(container);

var ctx = document.createElement('canvas').getContext('2d'),
    renderer = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true
    });

document.getElementById("canvas").appendChild(renderer.domElement);


renderer.domElement.style.zIndex = "100";
renderer.domElement.style.position = ctx.canvas.style.position = 'absolute';


function resize() {
    var ratio = 16 / 9,
        preHeight = window.innerWidth / ratio;

    if (preHeight <= window.innerHeight) {
        renderer.setSize(window.innerWidth, preHeight);
        ctx.canvas.width = window.innerWidth;
        ctx.canvas.height = preHeight;
    } else {
        var newWidth = Math.floor(window.innerWidth - (preHeight - window.innerHeight) * ratio);
        newWidth -= newWidth % 2 !== 0 ? 1 : 0;
        renderer.setSize(newWidth, newWidth / ratio);
        ctx.canvas.width = newWidth;
        ctx.canvas.height = newWidth / ratio;
    }

    renderer.domElement.style.width = '';
    renderer.domElement.style.height = '';
    renderer.domElement.style.left = ctx.canvas.style.left = (window.innerWidth - renderer.domElement.width) / 2 + 'px';
    renderer.domElement.style.top = ctx.canvas.style.top = (window.innerHeight - renderer.domElement.height) / 2 + 'px';
}

window.addEventListener('resize', resize);

resize();

//Scene and Camera
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(
    20, // Field of view
    16/9,
    0.1, // Near plane
    10000 // Far plane
);

camera.position.set(700, 335, 0);

var controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 1;
controls.enabled = false;

//Objects
var starColor = (function() {
        var colors = [0xFFFF00, 0x559999, 0xFF6339, 0xFFFFFF];
        return colors[Math.floor(Math.random() * colors.length)];
    })(),
    star = new THREE.Mesh(
        new THREE.IcosahedronGeometry(7, 1),
        new THREE.MeshBasicMaterial({
            color: 0xececec,
        })
    ),
    glows = [];

star.castShadow = false;

var orbitRadius = 70;
var orbits = [];
for (var k = 0; k < 6; k++ ){
    var orbit = new THREE.Line(
        new THREE.CircleGeometry(orbitRadius, 120),
        new THREE.MeshBasicMaterial({
            color: 0xffffff,
            transparent: true,
            opacity: .1,
            side: THREE.Backside
        })
    );
    orbit.geometry.vertices.shift();
    orbit.rotation.x = THREE.Math.degToRad(90);
    orbitRadius +=25;
    orbits.push(orbit);
    scene.add(orbit);
}

var geometry = new THREE.PlaneGeometry( 90, 71 );
var texture = new THREE.TextureLoader().load( '5.png' );
var material = new THREE.MeshPhongMaterial( { map: texture } );
material.transparent = true;
var plane = new THREE.Mesh( geometry, material );
plane.receiveShadow = true;
scene.add( plane );

var planets = [];
var geometry = new THREE.PlaneGeometry(45,65.625);
var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta_nowa1.png' );
var material = new THREE.MeshPhongMaterial({map:texture});
material.transparent = true;
material.map.minFilter = THREE.LinearFilter;
var planet1 = new THREE.Mesh( geometry, material);
//planet1.userData = {URL:"http://pp-bialoleka.malykopernik.pl"};
planet1.userData = {URL:"http://np-bialoleka.malykopernik.pl"};
planet1.orbitSpeed = 0.001;
planet1.orbit = 50;
planet1.name ="planet1";
planets.push(planet1);
scene.add(planet1);

var geometry = new THREE.PlaneGeometry(45,65.625);
var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta_nowa2.png' );
var material = new THREE.MeshPhongMaterial({map:texture});
material.transparent = true;
material.map.minFilter = THREE.LinearFilter;
var planet2 = new THREE.Mesh( geometry, material);
//planet2.userData = {URL:"http://pp-mokotow.malykopernik.pl"};
planet2.userData = {URL:"http://mokotow.malykopernik.pl"};
planet2.orbitSpeed = 0.001;
planet2.orbit = 550;
planet2.name ="planet2";
planets.push(planet2);
scene.add(planet2);

var geometry = new THREE.PlaneGeometry(45,65.625);
var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta_nowa3.png' );
var material = new THREE.MeshPhongMaterial({map:texture});
material.transparent = true;
material.map.minFilter = THREE.LinearFilter;
var planet3 = new THREE.Mesh( geometry, material);
planet3.userData = {URL:"http://np-bialoleka.malykopernik.pl"};
planet3.orbitSpeed = 0.001;
planet3.orbit = 300;
planet3.name ="planet3";
planets.push(planet3);
scene.add(planet3);

var geometry = new THREE.PlaneGeometry(45,65.625);
var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta_nowa4.png' );
var material = new THREE.MeshPhongMaterial({map:texture});
material.transparent = true;
material.map.minFilter = THREE.LinearFilter;
var planet4 = new THREE.Mesh( geometry, material);
//planet4.userData = {URL:"http://np-zabki.malykopernik.pl"};
planet4.userData = {URL:"http://zabki.malykopernik.pl"};
planet4.orbitSpeed = 0.001;
planet4.orbit = 900;
planet4.name ="planet4";
planets.push(planet4);
scene.add(planet4);

var geometry = new THREE.PlaneGeometry(45,65.625);
var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta_nowa5.png' );
var material = new THREE.MeshPhongMaterial({map:texture});
material.transparent = true;
material.map.minFilter = THREE.LinearFilter;
var planet5 = new THREE.Mesh( geometry, material);
planet5.userData = {URL:"http://radzymin.malykopernik.pl"};
//planet5.userData = {URL:"http://pp-radzymin.malykopernik.pl"};
planet5.orbitSpeed = 0.001;
planet5.orbit = 700;
planet5.name ="planet5";
planets.push(planet5);
scene.add(planet5);

var geometry = new THREE.PlaneGeometry(45,65.625);
var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta_nowa6.png' );
var material = new THREE.MeshPhongMaterial({map:texture});
material.transparent = true;
material.map.minFilter = THREE.LinearFilter;
var planet6 = new THREE.Mesh( geometry, material);
//planet6.userData = {URL:"http://pp-radzymin2.malykopernik.pl"};
planet6.userData = {URL:"http://radzymin.malykopernik.pl"};
planet6.orbitSpeed = 0.001;
planet6.orbit = 1100;
planet6.name ="planet6";
planets.push(planet6);
scene.add(planet6);


/*for( var z=0; z<6; z++){
    var geometry = new THREE.PlaneGeometry(45,65.625);
    var texture = new THREE.TextureLoader().load( 'planety_z_adresami/planeta'+(z+1)+'.png' );
    var material = new THREE.MeshPhongMaterial({map:texture});
    material.transparent = true;
    material.map.minFilter = THREE.LinearFilter;
    var planet = new THREE.Mesh( geometry, material);
    //planet.userData = {URL:"/"+z};
    planet.userData = {URL:"http://przedszkole1.przedszkole.ovh"};
    planet.orbitSpeed = 0.001;
    planet.orbit = Math.random() * Math.PI / 2 * z;
    planets.push(planet);
    scene.add(planet);
}*/

var canvasDiv = document.querySelector("canvas");
canvasDiv.addEventListener('mousedown', onDocumentMouseDown, false);
canvasDiv.addEventListener('mouseover', onDocumentMouseHover, false);


//Lights
var light1 = new THREE.PointLight(starColor, 2, 0, 0);
console.log('light2');
light1.position.set(600, 100, 0);
scene.add(light1);

var light3 = new THREE.AmbientLight(0xffffff);
scene.add(light3);

//2D
var bgStars = [];

for (var i = 0; i < 500; i++) {
    var tw = {
        x: Math.random(),
        y: Math.random()
    }
    bgStars.push(tw);
}


//Main Loop
var t = 0;
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

function animate() {
    //var pos = 145;
    /*for (var q in planets){
        var pl = planets[q];
        pl.orbit += pl.orbitSpeed;
        pl.position.set(Math.cos(pl.orbit) * pos, 10, Math.sin(pl.orbit) * pos);
        pos += 25;
    }*/

    planet1.orbit += planet1.orbitSpeed;
    planet1.position.set(Math.cos(planet1.orbit) * 145, 10, Math.sin(planet1.orbit) * 145);

    planet2.orbit += planet2.orbitSpeed;
    planet2.position.set(Math.cos(planet2.orbit) * 145, 10, Math.sin(planet2.orbit) * 145);

    planet3.orbit += planet3.orbitSpeed;
    planet3.position.set(Math.cos(planet3.orbit) * 165, 10, Math.sin(planet3.orbit) * 165);

    planet4.orbit += planet4.orbitSpeed;
    planet4.position.set(Math.cos(planet4.orbit) * 165, 10, Math.sin(planet4.orbit) * 165);

    planet5.orbit += planet5.orbitSpeed;
    planet5.position.set(Math.cos(planet5.orbit) * 195, 10, Math.sin(planet5.orbit) * 195);

    planet6.orbit += planet6.orbitSpeed;
    planet6.position.set(Math.cos(planet6.orbit) * 195, 10, Math.sin(planet6.orbit) * 195);




    t += 0.01;
    scene.rotation.y = 90 * Math.PI/180;
    for (var g in glows) {
        var glow = glows[g];
        glow.scale.set(
            Math.max(glow.origScale.x - .2, Math.min(glow.origScale.x + .2, glow.scale.x + (Math.random() > .5 ? 0.005 : -0.005))),
            Math.max(glow.origScale.y - .2, Math.min(glow.origScale.y + .2, glow.scale.y + (Math.random() > .5 ? 0.005 : -0.005))),
            Math.max(glow.origScale.z - .2, Math.min(glow.origScale.z + .2, glow.scale.z + (Math.random() > .5 ? 0.005 : -0.005)))
        );
        glow.rotation.set(0, t, 0);
    }
    renderer.shadowMap.enabled = true;
    renderer.shadowMapDarkness = 1;
    renderer.setClearColor( 0x000000, 0 );



    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
}

animate();

function onDocumentMouseHover(event1){
    /*event1.preventDefault();
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children );
    for ( var i = 0; i < intersects.length; i++ ) {
        if(intersects[i].object.name != ""){
            document.body.style.cursor = "pointer";
        }
    }*/
}


function onDocumentMouseDown(event) {
    event.preventDefault();
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children );
    for ( var i = 0; i < intersects.length; i++ ) {
        if(intersects[i].object.name != ""){
            window.location.href = intersects[i].object.userData.URL;
        }
    }
}
