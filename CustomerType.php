<!--
Plik formularza, Symfony 4.3, dodawanie Klienta
-->
<?php
// src/Form/CustomerType.php
namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CustomerType extends AbstractType
{
    private $authorization;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorization = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('images', CollectionType::class, [
            'entry_type' => ImageType::class,
            'prototype' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'required' => false,
            'label' => false,
        ]);

        $builder
            ->add('shortname', TextType::class, [
                'label' => 'Nazwa skrócona',
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'label' => 'Nazwa klienta'
            ])
            ->add('address', TextType::class, [
                'label' => 'Pełny adres',
                'required' => false,
            ])
            ->add('nip', TextType::class, [
                'label' => 'NIP',
                'required' => false,
            ])
            ->add('phone', TextType::class, [
                'label' => 'Telefon',
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adres email',
                'required' => false,
            ])
            ->add('notes', TextareaType::class, [
                'attr' => [
                    'class' => 'ckeditor'
                ],
                'label' => 'Notatki',
                'required' => false,
            ])
            ->add('contactPerson', CollectionType::class, [
                'entry_type' => ContactPersonType::class,
                'entry_options' => array('label' => false),
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Customer::class,
        ));
    }
}
