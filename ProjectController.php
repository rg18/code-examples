<!--
Jeden pliku z systemu CRM, Symfony 4.3, kontroler odpowiadający za obsługę klasy Project,
akcje: listowanie wszystkiech projektów, dodawanie, edytowanie, pokaż zawartość projektu,
usuń, pobranie osób kontaktowych przypisanych do danego projektu, zmiany statusu projekty,
dodanie czasu pracy do projektu oraz kopiowanie projektu.
-->
<?php

namespace App\Controller\Admin;

use App\Entity\ContactPerson;
use App\Entity\Project;
use App\Entity\User;
use App\Entity\WorkingTime;
use App\Form\ProjectSearchType;
use App\Form\ProjectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    /**
     * Projects listing
     * @Route("/panel/projects", name="projects")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function projectIndexAction()
    {
        $repository = $this->getDoctrine()->getRepository(Project::class);
        if ($this->isGranted(['ROLE_ADMIN'])) {
            $projects = $repository->findActive();
        } else {
            $projects = $repository->findActiveByUser($this->getUser()->getId());
        }

        $searchForm = $this->createForm(ProjectSearchType::class);
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isActive' => true]);

        return $this->render('admin/security/project/show.html.twig', [
            'projects' => $projects,
            'searchForm' => $searchForm->createView(),
            'users' => $users,
        ]);
    }

    /**
     * @Route("/panel/project/add", name="add_project")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function projectAddAction(Request $request)
    {
        $project = new Project();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProjectType::class, $project);
        if ($request->get('user_id')) {
            $form->get('users')->setData([$em->getRepository(User::class)->find((int)$request->get('user_id'))]);
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($project);
            $em->flush();
            return $this->redirectToRoute('projects');
        }

        return $this->render(
            'admin/security/project/add.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/panel/project/edit/{id},{customerid},{userid}", name="project_edit")
     * @ParamConverter("id", class="App:Project")
     * @Security("has_role('ROLE_USER')")
     */
    public function editProjectAction(Project $project, Request $request)
    {
        $form = $this->createForm(ProjectType::class, $project);
        $em = $this->getDoctrine()->getManager();
        if ($request->get('user_id')) {
            $form->get('users')->setData([$em->getRepository(User::class)->find((int)$request->get('user_id'))]);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('notice', 'Zmiany zapisano - ' . $project->getId());

            return $this->redirectToRoute($this->getRouteForProjectStatus($project->getStatus()), ['customer_id' => $request->get('customerid'), 'user_id' => $request->get('userid')]);
        }

        return $this->render('admin/security/project/edit.html.twig', [
            'form' => $form->createView(),
            'project' => $project,
        ]);
    }

    /**
     * @Route("/panel/project/show/{id}", name="project_show")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function showProjectAction(Project $project)
    {
        return $this->render('admin/security/project/show_one.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @Route("/panel/project/customer/{id}", name="customer_contact_person")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function showCustomerContactsAction($id)
    {
        $contactPerson = $this->getDoctrine()->getRepository(ContactPerson::class)->findBy(['customer' => $id]);
        foreach ($contactPerson as $contact) {
            $output[] = array($contact->getId(), $contact->getName());
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/panel/project/remove", name="project_remove")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function removeProjectAction(Request $request)
    {
        $projectId = $request->request->all()['projectId'];

        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find((int)$projectId);

        $em->remove($project);
        $em->flush();

        return new JsonResponse(['projectId' => $project->getId()]);
    }

    /**
     * @Route("/panel/projects/archive", name="projects_archive")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function archivedProjectsIndex()
    {
        $repository = $this->getDoctrine()->getRepository(Project::class);
        if ($this->isGranted(['ROLE_ADMIN'])) {
            $projects = $repository->findArchived();
        } else {
            $projects = $repository->findArchived($this->getUser()->getId());
        }
        $searchForm = $this->createForm(ProjectSearchType::class)->add('statusFilter', HiddenType::class, [
            'attr' => ['value' => 'archived']
        ]);
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isActive' => true]);

        return $this->render('admin/security/project/archived.html.twig', [
            'projects' => $projects,
            'searchForm' => $searchForm->createView(),
            'users' => $users,
        ]);
    }

    /**
     * @Route("/panel/projects/for-invoice", name="projects_for_invoice")
     * @Security("has_role('ROLE_USER')")
     */
    public function forInvoiceProjectsIndex()
    {
        $em = $this->getDoctrine()->getManager();
        $projects = $em->getRepository(Project::class)->findForInvoice();
        $searchForm = $this->createForm(ProjectSearchType::class)->add('statusFilter', HiddenType::class, [
            'attr' => ['value' => 'for_invoice']
        ]);
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['isActive' => true]);

        return $this->render('admin/security/project/for_invoice.html.twig', [
            'projects' => $projects,
            'searchForm' => $searchForm->createView(),
            'users' => $users,
        ]);
    }

    /**
     * @Route("/panel/project/change-status", name="project_change_status")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function changeStatusAjax(Request $request)
    {
        $data = $request->request->all();
        $projectId = (int)$data['projectId'];
        $statusId = (int)$data['statusId'];

        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository(Project::class)->find($projectId);
        $project->setStatus($statusId);

        $em->flush();

        return new JsonResponse(['projectId' => $projectId]);
    }

    /**
     * @Route("/panel/project/change-asap", name="project_change_asap")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function changeAsapAjax(Request $request)
    {
        $data = $request->request->all();
        $projectId = (int)$data['projectId'];
        $asap = $data['asap'];
        $asap = $asap == 'false' ? false : true;
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($projectId);
        $project->setAsap($asap);
        $em->flush();

        return new JsonResponse(['projectId' => $projectId]);
    }

    /**
     * @Route("/panel/project/render-all", name="projects_render_all")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function renderAllProjects(Request $request)
    {
        $data = $request->request->all();
        //var_dump($data);die;

        $repository = $this->getDoctrine()->getRepository(Project::class);
        $userId = null;
        if (!$this->isGranted('ROLE_ADMIN')) {
            $userId = $this->getUser()->getId();
            $data['project_search']['assignedTo'] = $userId;
        }
        if (isset($data['project_search'])) {
            if (isset($data['project_search']['statusFilter'])) {
                $projectsAsap = null;
            }
            $projects = $repository->search($data['project_search']);
        } else {
            if (isset($data['userId'])) {
                $projects = $repository->findActiveByUser($userId);
            } else {
                $projects = $repository->findActive();
            }
        }

        return $this->render('admin/security/project/projects.html.twig', [
            'projects' => $projects,
        ]);
    }

    /**
     * @Route("/panel/project/render", name="project_render")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function renderSingleProject(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $projectId = (int)$request->request->all()['projectId'];
        $loopIndex = (int)$request->request->all()['loopIndex'];

        $project = $em->getRepository(Project::class)->find($projectId);

        if (!$project->isActive()) {
            return new Response('remove');
        }

        return $this->render('admin/security/project/_project.html.twig', [
            'project' => $project,
            'loopIndex' => $loopIndex,
        ]);
    }

    /**
     * @Route("/panel/project/add-time", name="project_add_time")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function addTime(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $projectId = (int)$request->request->all()['projectId'];
        $time = (int)$request->request->all()['time'];

        $project = $em->getRepository(Project::class)->find($projectId);

        $workingTime = new WorkingTime();
        $workingTime->setProject($project);
        $workingTime->setUser($this->getUser());
        $workingTime->setTimeStart(new \DateTime());
        $workingTime->setTimeFinish(new \DateTime());
        $workingTime->setTimeDiff($time);
        $workingTime->getProject()->setTimeSpent($workingTime->getProject()->getTimeSpent() + $time);

        $em->persist($workingTime);
        $em->flush();

        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/panel/project/copy", name="project_copy")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')"))
     */
    public function copyProject(Request $request)
    {
        $data = $request->request->all();
        $projectId = (int)$data['projectId'];

        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($projectId);

        $newProject = clone $project;
        $newProject->clearWorkingTimes();
        foreach ($project->getTasks() as $task) {
            $newTask = clone $task;
            $newTask->setProject($newProject);
            $newProject->addTask($newTask);
        }
        foreach ($project->getCyclicTasks() as $cyclicTask) {
            $newTask = clone $cyclicTask;
            $newTask->setProject($newProject);
            $newProject->addCyclicTask($newTask);
        }
        foreach ($project->getInvoiceItems() as $invoiceItem) {
            $newInvoiceItem = clone $invoiceItem;
            $newInvoiceItem->setProject($newProject);
            $newProject->addInvoiceItem($newInvoiceItem);
        }
        $newProject->setCreatedAt(new \DateTime());
        $newProject->setUpdatedAt(null);

        $em->persist($newProject);
        $em->flush();

        return new JsonResponse(['projectId' => $newProject->getId()]);
    }

    /**
     * @param int $status
     * @return string
     */
    private function getRouteForProjectStatus(int $status)
    {
        if (in_array($status, [Project::STATUS_CLOSED, Project::STATUS_CLOSED_ABANDONED])) {
            return 'projects_archive';
        } elseif ($status === Project::STATUS_CLOSED_FOR_INVOICE) {
            return 'projects_for_invoice';
        } else {
            return 'projects';
        }
    }
}
